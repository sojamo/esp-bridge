#include <SoftwareSerial.h>
SoftwareSerial mySerial(10, 11); // RX, TX

void setup() {

  Serial.begin(57600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }
  Serial.println("Goodnight moon!");

  // set the data rate for the SoftwareSerial port
  mySerial.begin(115200);
  mySerial.println("Hello, world?");

  pinMode(13,OUTPUT);
  digitalWrite(13,HIGH);
  
}

void loop() {

  if (mySerial.available()) {
    while (mySerial.available() > 0) {
      char inByte = mySerial.read();
      Serial.write(inByte);
      if(inByte=='1') {
        digitalWrite(13,HIGH);
        delay(10);
        digitalWrite(13,LOW);
      }
    }
  }
  
  
  
}
