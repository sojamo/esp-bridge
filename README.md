# Using the ESP as a serial communication bridge over UDP

This repo contains source code for an arduino or teensy based microcontrolle which sends and receives serial data. The ESP then handles the wifi connection and Serial-to-UDP communication.

Here the ESP is running a nodemcu firmware that enables the ESP to run lua scripts. Due to the long upload time of other firmware types (for example the arduino based firmware), the nodemcu firmware is preferred since uploading and updating lua scripts is much more upload-time efficient. lua scripts can be uploaded to an ESP using the [ESPlorer](http://esp8266.ru/esplorer/) ([github](https://github.com/4refr0nt/ESPlorer)) software.



