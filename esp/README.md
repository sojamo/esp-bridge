# Uploading firmware to the ESP8266

The firmware used on the esp should be a [nodemcu](https://github.com/nodemcu/nodemcu-firmware) firmware. You can use the cloud build service to build your custom firmware [here](http://nodemcu-build.com/). Other options such as Linux and Docker are documented [here](https://nodemcu.readthedocs.io/en/dev/en/build).

To upload the firmware to the esp, use the esptool that ships with platformio, use the commandline snippet shown below (make adjustments where necessary). 
When using an ESPresso board, hold down the reset and flash button, then execute the above command followed by releasing the reset button first and then the flash button.

```
/.platformio/packages/tool-esptool/esptool -vv -cd nodemcu -cb 115200 -cp "/dev/cu.usbserial-AJV9OOOW" -cf ./firmware/your-nodemcu-firmware.bin
```

build your _your-nodemcu-firmware.bin_ with the nodemcu build service.



