-- main program running on esp
--
-- starts a udp server listening on port 5000
-- and sends a command to the micro-controller connected
-- via a serial connecton when receiving a udp packet. 
-- Eventually incoming messages should be forwarded via 
-- serial here. Also serial messages should then be 
-- forwarded via the UDP socket.
--
--



uart.setup(0, 9600, 8, uart.PARITY_NONE, uart.STOPBITS_1, 1)
uart.alt(1)

uart.on("data", "\n",
  function(data)
    print("receive from uart:", data)
    if data=="quit\r" then
      uart.on("data") -- unregister callback function
    end
  end, 
0)

pin=0
port=5000
remoteAddress="192.168.1.194"
remotePort=5001

conn=net.createConnection(net.UDP,0)
conn:connect(remotePort, remoteAddress)

-- Note: it is also possible for the server to reply 
-- to a message and send a reply to the sender.

srv=net.createServer(net.UDP)
  srv:on("receive", function(srv,msg)
    if msg=="on" then 
      gpio.write(pin,gpio.HIGH) 
      uart.write(0, "0\n")
    else 
      gpio.write(pin,gpio.LOW) 
      uart.write(0, "1\n")
      conn:send("ok.")
    end
  end)

srv:listen(port)




