--
-- ESP-wifi-to-serial bridge
--

function start()
  -- list all files
  l=file.list()
  for k,v in pairs(l) do 
    print("name:"..k..", size:"..v) 
  end

  flash(3)
  -- connect to wifi

  print("# Starting espresso, waiting for wifi connection ... ")

  ap = "access-point"
  pw = "password"

  wifi.setmode(wifi.STATION)
  wifi.sta.config(ap,pw)
  wifi.sta.connect()
  tmr.alarm(1, 1000, 1, function()
    if wifi.sta.getip()== nil then
      print("# IP unavailable, continue waiting for access-point "..ap)
    else
      tmr.stop(1)
      print("# ESP8266 mode is: " .. wifi.getmode())
      print("# The module MAC address is: " .. wifi.ap.getmac())
      print("# Config done, IP is "..wifi.sta.getip())

      flash(5)

      -- start main routine
      dofile ("main.lua")
    end
  end)
end

function flash(n)
  pin = 0  -- flash D1 (pin 0) led 4 times
  for i=1,n do
    gpio.write(pin, gpio.LOW)
    tmr.delay(100000)
    gpio.write(pin, gpio.HIGH)
    tmr.delay(100000)
  end
end

-- start
start()


